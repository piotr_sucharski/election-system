pragma solidity 0.4.25;


contract ElectionSystem {
    
    address public owner;
    
    mapping(address => bool) public citizenHasVoted;
    
    
    uint256 endDate;
    
    Candidate[] public candidates;
    
    struct Candidate {
        string name;
        uint256 votesCount;
    }
    
    constructor(uint256 duration) {
        owner = msg.sender;
        endDate = now + duration;
    }
    
    function addCandidate(string newCandidateName) public onlyOwner {
        
        Candidate memory newCandidate = Candidate(newCandidateName, 0);
        
        candidates.push(newCandidate);
    }
    
    function vote(uint8 candidateID) public isVotingOpen hasAlreadyVoted {
        ++candidates[candidateID].votesCount;
        citizenHasVoted[msg.sender] = true;
    }
    
    modifier onlyOwner() {
        require(msg.sender == owner, "Only contract owner can add candidates.");
        _;
    }
    
    modifier hasAlreadyVoted() {
        require(citizenHasVoted[msg.sender] == false, "This citizen has already voted");
        _;
    }
    
    modifier isVotingOpen() {
        require(now < endDate, "Voting is closed");
        _;
    }
}